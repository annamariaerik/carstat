package ee.datanor.carstat.dao;

import ee.datanor.carstat.dto.SearchCarDto;
import ee.datanor.carstat.util.SearchCriteria;
import ee.datanor.carstat.domain.Car;

import java.util.List;

public interface ICarDao {
    SearchCarDto searchCar(List<SearchCriteria> params, int page, int limit);
}