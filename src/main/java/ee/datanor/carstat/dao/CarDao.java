package ee.datanor.carstat.dao;

import ee.datanor.carstat.dto.SearchCarDto;
import ee.datanor.carstat.util.SearchCriteria;
import ee.datanor.carstat.domain.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class CarDao implements ICarDao {
    @Autowired
    private EntityManager entityManager;

    @Override
    public SearchCarDto searchCar(final List<SearchCriteria> params, int page, int limit) {
        final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Car> query = builder.createQuery(Car.class);
        final Root r = query.from(Car.class);

        Predicate predicate = builder.conjunction();
        CarSearchCriteriaConsumer searchConsumer = new CarSearchCriteriaConsumer(predicate, builder, r);
        params.forEach(searchConsumer);
        predicate = searchConsumer.getPredicate();
        query.where(predicate);
        Query query1 = entityManager.createQuery(query);

        SearchCarDto searchCarDto = new SearchCarDto();
        searchCarDto.setTotal(query1.getResultList().size());

        query1.setFirstResult((page - 1) * limit);
        query1.setMaxResults(limit);
        searchCarDto.setResult(query1.getResultList());

        return searchCarDto;
    }
}