package ee.datanor.carstat.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@SequenceGenerator(name = "id_seq", sequenceName = "car_seq", allocationSize = 1)
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_seq")
    private long id;

    @Column
    private String mark;

    @Column
    private String model;

    @Column
    private String carBodyType;

    @Column
    private int issueYear;

    @Column
    private String color;

    @Column
    private String engineType;

    @Column
    private int engineVolume;

    @Column
    private int enginePower;

    @Column
    private String gearbox;

    @Column
    private int quantity;

    @Column
    private Integer weight;
}