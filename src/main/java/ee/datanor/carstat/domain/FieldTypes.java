package ee.datanor.carstat.domain;

import ee.datanor.carstat.util.ValidationCase;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@SequenceGenerator(name = "id_seq", sequenceName = "field_type_seq", allocationSize = 1)
public class FieldTypes {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_seq")
    private long id;

    @Column
    @Enumerated(EnumType.STRING)
    private ValidationCase field;

    @Column
    private String fieldValue;

    @Column
    private String replace;
}