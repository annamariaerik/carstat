package ee.datanor.carstat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ee.datanor.carstat")
@EnableAutoConfiguration
@SpringBootApplication
public class CarstatApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarstatApplication.class, args);
	}

}
