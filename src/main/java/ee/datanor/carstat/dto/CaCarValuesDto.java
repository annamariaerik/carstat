package ee.datanor.carstat.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CaCarValuesDto {
    @JsonProperty("Name")
    private String name;

    @JsonProperty("Value")
    private String value;
}
