package ee.datanor.carstat.dto;

import ee.datanor.carstat.util.ValidationCase;
import lombok.Data;

import javax.persistence.Column;

@Data
public class FieldTypesDto {
    private String field;
    private String fieldValue;
    private String replace;
}
