package ee.datanor.carstat.dto;

import ee.datanor.carstat.domain.Car;
import lombok.Data;

import java.util.List;
@Data
public class SearchCarDto {
    private List<Car> result;
    private long total;
}
