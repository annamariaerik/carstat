package ee.datanor.carstat.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CarGroupedDto {
    private String mark;
    private String model;
    private Integer issueYear;
    private String color;
    private long quantity;

    public CarGroupedDto(String mark, String model, Integer issueYear, long quantity) {
        this.mark = mark;
        this.model = model;
        this.issueYear = issueYear;
        this.quantity = quantity;
    }

    public CarGroupedDto(String mark, String model, String color, long quantity) {
        this.mark = mark;
        this.model = model;
        this.color = color;
        this.quantity = quantity;
    }

    public CarGroupedDto(String mark, String model, long quantity) {
        this.mark = mark;
        this.model = model;
        this.quantity = quantity;
    }
}