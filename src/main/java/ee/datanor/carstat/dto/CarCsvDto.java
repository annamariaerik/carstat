package ee.datanor.carstat.dto;

import com.opencsv.bean.CsvBindByPosition;
import lombok.Data;

@Data
public class CarCsvDto {
    private long id;

    @CsvBindByPosition(position = 1)
    private String mark;

    @CsvBindByPosition(position = 2)
    private String model;

    @CsvBindByPosition(position = 3)
    private String carBodyType;

    @CsvBindByPosition(position = 4)
    private String issueYear;

    @CsvBindByPosition(position = 5)
    private String color;

    @CsvBindByPosition(position = 6)
    private String engineType;

    @CsvBindByPosition(position = 8)
    private int engineVolume;

    @CsvBindByPosition(position = 9)
    private String enginePower;

    @CsvBindByPosition(position = 10)
    private String gearbox;

    @CsvBindByPosition(position = 13)
    private int quantity;
}
