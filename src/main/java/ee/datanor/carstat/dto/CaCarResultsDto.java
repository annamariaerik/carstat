package ee.datanor.carstat.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CaCarResultsDto {
    @JsonProperty("Count")
    private Integer count;

    @JsonProperty("Message")
    private String message;

    @JsonProperty("Results")
    private List<CaCarSpecsDto> results;

    @JsonProperty("SearchCriteria")
    private String searchCriteria;
}