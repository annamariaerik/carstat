package ee.datanor.carstat.dto.Chart;

import ee.datanor.carstat.dto.Chart.CarMarkDto;
import lombok.Data;

import java.util.List;

@Data
public class CarTreeMapDto {
    private String name;
    private List<CarMarkDto> children;
}
