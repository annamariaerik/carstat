package ee.datanor.carstat.dto.Chart;

import lombok.Data;

import java.util.List;
@Data
public class CarMarkDto {
    private String name;
    private List<CarModelDto> children;
    private long size;

    public CarMarkDto(String name, long size) {
        this.name = name;
        this.size = size;
    }
}
