package ee.datanor.carstat.dto.Chart;

import lombok.Data;

import java.util.List;

@Data
public class CarModelDto {
    private String name;
    private long size;
    private List<CarYearDto> children;

    public CarModelDto(String name, long size) {
        this.name = name;
        this.size = size;
    }
}
