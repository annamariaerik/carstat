package ee.datanor.carstat.dto.Chart;

import lombok.Data;

@Data
public class CarYearDto {
    private String name;
    private long value;
    private int year;

    public CarYearDto(int year, long value) {
        this.year = year;
        this.value = value;
    }

    public CarYearDto() {}
}