package ee.datanor.carstat.repository;

import ee.datanor.carstat.domain.Car;
import ee.datanor.carstat.dto.CarGroupedDto;
import ee.datanor.carstat.dto.SearchCarDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CarStatisticsRepository {

    @Autowired
    private EntityManager entityManager;

    public SearchCarDto findCars(String mark, String model, Integer issueYear, String color, String carBodyType, String engineType, Integer enginePower, int page, int limit) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Car> criteriaQuery = criteriaBuilder.createQuery(Car.class);

        Root<Car> carRoot = criteriaQuery.from(Car.class);

        List<Predicate> predicates = new ArrayList<>();
        if (color != null && !color.isEmpty()) {
            predicates.add(criteriaBuilder.equal(carRoot.get("color"), color));
        }
        if (mark != null && !mark.isEmpty()) {
            predicates.add(criteriaBuilder.equal(carRoot.get("mark"), mark));
        }
        if (model != null && !model.isEmpty()) {
            predicates.add(criteriaBuilder.equal(carRoot.get("model"), model));
        }
        if (carBodyType != null && !carBodyType.isEmpty()) {
            predicates.add(criteriaBuilder.equal(carRoot.get("carBodyType"), carBodyType));
        }
        if (engineType != null && !engineType.isEmpty()) {
            predicates.add(criteriaBuilder.equal(carRoot.get("engineType"), engineType));
        }
        if (issueYear != null) {
            predicates.add(criteriaBuilder.equal(carRoot.get("issueYear"), issueYear));
        }
        if (enginePower != null) {
            predicates.add(criteriaBuilder.equal(carRoot.get("enginePower"), enginePower));
        }
        criteriaQuery.where(predicates.toArray(new Predicate[0]));

        TypedQuery<Car> query = entityManager.createQuery(criteriaQuery);

        SearchCarDto searchCarDto = new SearchCarDto();
        searchCarDto.setTotal(query.getResultList().size());

        query.setFirstResult((page - 1) * limit);
        query.setMaxResults(limit);

        searchCarDto.setResult(query.getResultList());

        return searchCarDto;
    }
}
