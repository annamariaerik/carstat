package ee.datanor.carstat.repository;

import ee.datanor.carstat.domain.Car;
import ee.datanor.carstat.domain.FieldTypes;
import ee.datanor.carstat.service.ValidateDataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;

@Slf4j
@Repository
public class ValidateDataRepository {

    @Autowired
    ValidateDataService validateDataService;

    @Autowired
    EntityManager entityManager;

    public Query updateField(String field, FieldTypes fieldTypes) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaUpdate<Car> carCriteriaUpdate = criteriaBuilder.createCriteriaUpdate(Car.class);
        Root<Car> carRoot = carCriteriaUpdate.from(Car.class);

        carCriteriaUpdate.set(carRoot.get(field), fieldTypes.getFieldValue());
        carCriteriaUpdate.where(criteriaBuilder.equal(carRoot.get(field), fieldTypes.getReplace()));

        return entityManager.createQuery(carCriteriaUpdate);
    }

    @Transactional
    public void updateCar(FieldTypes fieldTypes) {
        updateField(validateDataService.getField(fieldTypes), fieldTypes).executeUpdate();
    }
}
