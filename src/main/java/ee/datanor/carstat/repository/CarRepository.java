package ee.datanor.carstat.repository;

import ee.datanor.carstat.domain.Car;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRepository extends CrudRepository<Car, Long> {

    List<Car> findByIssueYear(int issueYear);

    List<Car> findByMarkIgnoreCase(String mark);

    List<Car> findByColor(String color);

    List<Car> findByColorAndMarkIgnoreCase(String color, String mark);

    List<Car> findByColorAndIssueYear(String color, int issueYear);

    List<Car> findByMarkIgnoreCaseAndIssueYear(String mark, int issueYear);

    List<Car> findByColorAndMarkIgnoreCaseAndIssueYear(String color, String mark, int issueYear);

    List<Car> findByMarkIgnoreCaseAndModelIgnoreCaseAndIssueYear(String mark, String model, int issueYear);
}
