package ee.datanor.carstat.repository;

import ee.datanor.carstat.domain.FieldTypes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FieldTypesRepository extends CrudRepository<FieldTypes, Long> {
}
