package ee.datanor.carstat.repository;

import ee.datanor.carstat.domain.Car;
import ee.datanor.carstat.dto.Chart.CarMarkDto;
import ee.datanor.carstat.dto.Chart.CarModelDto;
import ee.datanor.carstat.dto.Chart.CarYearDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ChartRepository {

    @Autowired
    private EntityManager entityManager;

    public List<CarMarkDto> findGroupedMarks() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<CarMarkDto> criteriaQuery = criteriaBuilder.createQuery(CarMarkDto.class);

        Root<Car> root = criteriaQuery.from(Car.class);

        criteriaQuery.multiselect(root.get("mark"), criteriaBuilder.sum(root.get("quantity")));
        criteriaQuery.groupBy(root.get("mark"));

        TypedQuery<CarMarkDto> typedQuery = entityManager.createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    public List<CarModelDto> findGroupedModels(String mark) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<CarModelDto> criteriaQuery = criteriaBuilder.createQuery(CarModelDto.class);

        Root<Car> root = criteriaQuery.from(Car.class);

        criteriaQuery.multiselect(root.get("model"), criteriaBuilder.sum(root.get("quantity")));
        criteriaQuery.where(criteriaBuilder.equal(root.get("mark"), mark));
        criteriaQuery.groupBy(root.get("model"));

        TypedQuery<CarModelDto> typedQuery = entityManager.createQuery(criteriaQuery);
        List<CarModelDto> carModelList = typedQuery.getResultList();

        return carModelList;
    }

    public List<CarYearDto> findGroupedYears(String model, String mark) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<CarYearDto> criteriaQuery = criteriaBuilder.createQuery(CarYearDto.class);

        Root<Car> root = criteriaQuery.from(Car.class);
        List<Predicate> predicates = new ArrayList<>();

        criteriaQuery.multiselect(root.get("issueYear"), criteriaBuilder.sum(root.get("quantity")));
        predicates.add(criteriaBuilder.equal(root.get("model"), model));
        predicates.add(criteriaBuilder.equal(root.get("mark"), mark));

        criteriaQuery.where(predicates.toArray(new Predicate[0]));
        criteriaQuery.groupBy(root.get("issueYear"));

        TypedQuery<CarYearDto> typedQuery = entityManager.createQuery(criteriaQuery);
        List<CarYearDto> carYearList = typedQuery.getResultList();

        return carYearList;
    }
}
