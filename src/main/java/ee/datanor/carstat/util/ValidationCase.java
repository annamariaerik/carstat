package ee.datanor.carstat.util;

public enum ValidationCase {
    MARK, MODEL, CAR_BODY_TYPE, COLOR, ENGINE_TYPE, GEARBOX;
}
