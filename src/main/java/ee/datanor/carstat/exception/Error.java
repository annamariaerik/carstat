package ee.datanor.carstat.exception;

public class Error {

    public enum Code {
        GENERAL_ERROR(Type.GENERAL),

        INVALID_INPUT_ERROR(Type.VALIDATION),
        ALREADY_EXISTS(Type.VALIDATION),
        NOT_FOUND(Type.NOT_FOUND),;

        private final Type type;

        Code(Type type) {
            this.type = type;
        }
    }

    public enum Type {
        GENERAL,
        VALIDATION,
        NOT_FOUND
    }
}
