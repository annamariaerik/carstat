package ee.datanor.carstat.exception;

import lombok.Data;

@Data
public class ErrorDto {
    private Error.Code code;
    private String message;

    public ErrorDto(Error.Code code, String message) {
        this.code = code;
        this.message = message;
    }
}
