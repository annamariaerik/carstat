package ee.datanor.carstat.exception;

import lombok.Data;

@Data
public class ValidationException extends RuntimeException {

    public ValidationException(String message, Error.Code code) {
        super(message);
        this.code = code;
    }

    Error.Code code;
}