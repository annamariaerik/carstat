package ee.datanor.carstat.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Locale;

@Slf4j
@ControllerAdvice
public class ExceptionHandlerAdvice {

    @ExceptionHandler(value = {Exception.class})
    public
    @ResponseBody
    ResponseEntity<ErrorDto> globalException(Exception ex) {
        log.warn("HTTP 500 - {}", ex);
        ErrorDto error = new ErrorDto(Error.Code.GENERAL_ERROR, ex.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).contentType(MediaType.APPLICATION_JSON).body(error);
    }

    @ExceptionHandler(value = {ValidationException.class})
    public
    @ResponseBody
    ResponseEntity<ErrorDto> handleValidationError(ValidationException ex) {
        log.warn("HTTP 401 - {}", ex.getMessage());
        ErrorDto error = new ErrorDto(ex.getCode(), ex.getMessage());
        return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).contentType(MediaType.APPLICATION_JSON).body(error);
    }

    @ExceptionHandler(value = {NotFoundException.class})
    public
    @ResponseBody
    ResponseEntity<ErrorDto> notFound(NotFoundException ex) {
        log.warn("HTTP 404 - {}", ex.getMessage());
        ErrorDto error = new ErrorDto(ex.getCode(), ex.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(MediaType.APPLICATION_JSON).body(error);
    }
}
