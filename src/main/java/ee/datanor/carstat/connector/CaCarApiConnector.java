package ee.datanor.carstat.connector;

import ee.datanor.carstat.dto.CaCarResultsDto;
import ee.datanor.carstat.exception.Error;
import ee.datanor.carstat.exception.ValidationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Service
public class CaCarApiConnector {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${canadianCar.api.url}")
    private String canadianCarApiUrl;

    public CaCarResultsDto getCaCarInfo(String mark, String model, String year) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(canadianCarApiUrl + "/vehicles/GetCanadianVehicleSpecifications/?Year=" + year + "&Make=" + mark + "&Model=" + model + "&units=metric&format=json");
        ResponseEntity<CaCarResultsDto> caCarList = restTemplate.getForEntity(builder.build(true).toUri(), CaCarResultsDto.class);
        if (caCarList.getStatusCode().is2xxSuccessful()) {
            return caCarList.getBody();
        } else {
            throw new ValidationException("It wasn't possible to retrieve data", Error.Code.NOT_FOUND);
        }
    }
}