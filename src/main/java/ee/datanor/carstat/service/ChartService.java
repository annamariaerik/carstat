package ee.datanor.carstat.service;

import ee.datanor.carstat.dto.Chart.*;
import ee.datanor.carstat.repository.ChartRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@Component
public class ChartService {
    @Autowired
    ChartRepository chartRepository;

    public CarTreeMapDto findAllMarkModelYear(String mapName) {
        List<CarMarkDto> carMarkList = chartRepository.findGroupedMarks();

        CarTreeMapDto carTreeMapDto = new CarTreeMapDto();
        carTreeMapDto.setChildren(carMarkList);
        carTreeMapDto.setName(mapName);

        for (CarMarkDto carMark : carMarkList) {
            for (int i = 0; i < carMarkList.size(); i++) {
                if (carTreeMapDto.getChildren().get(i).getName().equals(carMark.getName())) {
                    List<CarModelDto> carModelList = chartRepository.findGroupedModels(carMark.getName());
                    carTreeMapDto.getChildren().get(i).setChildren(carModelList);
                    for (CarModelDto carModel : carModelList) {
                        for (int j = 0; j < carModelList.size(); j++) {
                            if (carTreeMapDto.getChildren().get(i).getChildren().get(j).getName().equals(carModel.getName())) {
                                List<CarYearDto> carYearList = chartRepository.findGroupedYears(carModel.getName(), carMark.getName());
                                carTreeMapDto.getChildren().get(i).getChildren().get(j).setChildren(carYearList);

                                for (CarYearDto car : carYearList) {
                                    String yearText = car.getYear() + "a";
                                    car.setName(yearText);
                                }
                            }
                        }
                    }
                }
            }
        }
        return carTreeMapDto;
    }
}
