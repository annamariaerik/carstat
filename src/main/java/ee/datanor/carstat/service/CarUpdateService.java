package ee.datanor.carstat.service;

import ee.datanor.carstat.connector.CaCarApiConnector;
import ee.datanor.carstat.domain.Car;
import ee.datanor.carstat.dto.CaCarResultsDto;
import ee.datanor.carstat.exception.NotFoundException;
import ee.datanor.carstat.exception.ValidationException;
import ee.datanor.carstat.repository.CarRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static ee.datanor.carstat.exception.Error.Code.INVALID_INPUT_ERROR;
import static ee.datanor.carstat.exception.Error.Code.NOT_FOUND;

@Slf4j
@Service
public class CarUpdateService {

    @Autowired
    CaCarApiConnector caCarApiConnector;

    @Autowired
    CarRepository carRepository;

    @Autowired
    CarService carService;

    public CaCarResultsDto getCaCarInfo(String mark, String model, String year) {
        if (mark == null || model == null || year == null) {
            throw new ValidationException("All parameters are mandatory", INVALID_INPUT_ERROR);
        }
        if (!NumberUtils.isParsable(year)) {
            throw new ValidationException("Year must be number", INVALID_INPUT_ERROR);
        }
        return caCarApiConnector.getCaCarInfo(mark, model, year);
    }

    public int updateCarWeight(String mark, String model, String year) {
        CaCarResultsDto results = getCaCarInfo(mark, model, year);
        List<Car> carList = carService.findByMarkIgnoreCaseAndModelIgnoreCaseAndIssueYear(mark, model, Integer.parseInt(year));
        if (carList.size() < 1) {
            throw new NotFoundException("Sellise margi, mudeli ja/või aastaarvuga autosid ei ole tabelis", NOT_FOUND);
        }
        int count = 0;

        if (results.getResults().size() < 1) {
            throw new NotFoundException("Sellise margi, mudeli ja/või aastaarvuga autosid ei leitud api tulemustest", NOT_FOUND);
        }
        for (Car car : carList) {
            if (car.getMark().equalsIgnoreCase(mark) && car.getIssueYear() == Integer.parseInt(year) && car.getModel().equalsIgnoreCase(model)) {
                count++;
                car.setWeight(Integer.parseInt(results.getResults().get(0).getSpecs().get(7).getValue()));
                carRepository.save(car);
            }
        }
        return count;
    }
}