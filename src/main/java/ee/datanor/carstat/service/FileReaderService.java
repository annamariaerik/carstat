package ee.datanor.carstat.service;

import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import ee.datanor.carstat.domain.Car;
import ee.datanor.carstat.domain.FieldTypes;
import ee.datanor.carstat.dto.CarCsvDto;
import ee.datanor.carstat.repository.CarRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Slf4j
@Service
public class FileReaderService {
    private static final String CAR_LIST_CSV = "cars_list.csv";

    @Autowired
    CarRepository carRepository;

    @Autowired
    ValidateDataService validateDataService;

    public void importCar() throws IOException {

        Reader reader = new InputStreamReader(new ClassPathResource(CAR_LIST_CSV).getInputStream(), "ISO-8859-1");

        ColumnPositionMappingStrategy strategy = new ColumnPositionMappingStrategy();
        strategy.setType(CarCsvDto.class);

        CsvToBean<CarCsvDto> csvToBean = new CsvToBeanBuilder(reader).withType(CarCsvDto.class).withSkipLines(5).withSeparator(';').withIgnoreLeadingWhiteSpace(true).build();

        Iterator<CarCsvDto> carIterator = csvToBean.iterator();

        if (carIterator.hasNext()) {
            carRepository.deleteAll();
        }

        int count = 0;
        int countTotal = 0;
        List<Car> carsBatch = new ArrayList<>();
        List<FieldTypes> replaceValues = validateDataService.getFieldTypes();
        while (carIterator.hasNext()) {
            count++;
            countTotal++;

            CarCsvDto carCsvDto = carIterator.next();
            Car car = new Car();
            BeanUtils.copyProperties(carCsvDto, car);

            // Some eningePower is empty in CSV
            if (carCsvDto.getEnginePower() != null) {
                car.setEnginePower((int) Double.parseDouble(carCsvDto.getEnginePower()));
            }

            // Some IssueYear is "Määramata" in CSV
            if (NumberUtils.isParsable(carCsvDto.getIssueYear())) {
                car.setIssueYear(Integer.parseInt(carCsvDto.getIssueYear()));
            }

            // Save only these rows there mark and model is not null
            if (car.getMark() != null && car.getModel() != null) {

                // Convert data to uppercase
                car.setMark(carCsvDto.getMark().toUpperCase());
                car.setModel(carCsvDto.getModel().toUpperCase());
                car.setCarBodyType(carCsvDto.getCarBodyType().toUpperCase());
                car.setColor(carCsvDto.getColor().toUpperCase());
                car.setEngineType(carCsvDto.getEngineType().toUpperCase());
                if (carCsvDto.getGearbox() != null) {
                    car.setGearbox(carCsvDto.getGearbox().toUpperCase());
                }

                carsBatch.add(validateDataService.validateClassificators(car, replaceValues));

                if (count > 5000) {
                    carRepository.saveAll(carsBatch);
                    log.debug("andmebaasi salvestati {} kirjet", carsBatch.size());
                    count = 0;
                    carsBatch = new ArrayList<>();
                }
            }


//            // for testing not to import over 5000 item
//            if (countTotal > 5000) {
//                break;
//            }
        }
        if (carsBatch.size() != 0) {
            carRepository.saveAll(carsBatch);
        }
    }
}