package ee.datanor.carstat.service;

import com.google.common.collect.Lists;
import ee.datanor.carstat.domain.Car;

import ee.datanor.carstat.domain.FieldTypes;
import ee.datanor.carstat.dto.FieldTypesDto;
import ee.datanor.carstat.exception.Error;
import ee.datanor.carstat.exception.ValidationException;
import ee.datanor.carstat.repository.FieldTypesRepository;
import ee.datanor.carstat.repository.ValidateDataRepository;
import ee.datanor.carstat.util.ValidationCase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class ValidateDataService {

  @Autowired
  FieldTypesRepository fieldTypesRepository;

  @Autowired
  ValidateDataRepository validateDataRepository;

  public List<FieldTypes> getFieldTypes() {
    return Lists.newArrayList(fieldTypesRepository.findAll());
  }

  public Car validateClassificators(Car car, List<FieldTypes> replaceValues) {
    for (FieldTypes fieldTypes : replaceValues) {
      switch (fieldTypes.getField()) {
        case MARK:
          if (fieldTypes.getReplace().equals(car.getMark())) {
            car.setMark(fieldTypes.getFieldValue());
          }
        case MODEL:
          if (fieldTypes.getReplace().equals(car.getModel())) {
            car.setModel(fieldTypes.getFieldValue());
          }
        case CAR_BODY_TYPE:
          if (fieldTypes.getReplace().equals(car.getCarBodyType())) {
            car.setCarBodyType(fieldTypes.getFieldValue());
          }
        case COLOR:
          if (fieldTypes.getReplace().equals(car.getColor())) {
            car.setColor(fieldTypes.getFieldValue());
          }
        case ENGINE_TYPE:
          if (fieldTypes.getReplace().equals(car.getEngineType())) {
            car.setEngineType(fieldTypes.getFieldValue());
          }
        case GEARBOX:
          if (fieldTypes.getReplace().equals(car.getGearbox())) {
            car.setGearbox(fieldTypes.getFieldValue());
          }
      }
    }
    return car;
  }

  public Car validateClassificators(Car car) {
      return validateClassificators(car, getFieldTypes());
  }

  public void addValidation(FieldTypesDto fieldTypesDto) {
    validateDataRepository.updateCar(convertFieldTypesDtoToFieldTypesAndSave(fieldTypesDto));
  }

  public FieldTypes convertFieldTypesDtoToFieldTypesAndSave(FieldTypesDto fieldTypesDto) {
    FieldTypes fieldTypes = new FieldTypes();
    if (getEnum(fieldTypesDto) != null) {
      fieldTypes.setField(ValidationCase.valueOf(fieldTypesDto.getField()));
    }
    else if (getEnum(fieldTypesDto) == null) {
      throw new ValidationException("Field value have to be MARK, MODEL, CAR_BODY_TYPE, COLOR, ENGINE_TYPE or GEARBOX!", Error.Code.NOT_FOUND);
    }
    fieldTypes.setFieldValue(fieldTypesDto.getFieldValue());
    fieldTypes.setReplace(fieldTypesDto.getReplace());

    Optional<FieldTypes> fieldTypesNew = getFieldTypes().stream().filter(x -> fieldTypesDto.getReplace().equals(x.getReplace())).findAny();
    if (fieldTypesNew.isPresent()) {
      throw new ValidationException("This replace value already exists!", Error.Code.ALREADY_EXISTS);
    }
    else {
      fieldTypesRepository.save(fieldTypes);
    }
    return fieldTypes;
  }

  public String getField(FieldTypes fieldTypes) {
    switch (fieldTypes.getField()) {
      case MARK:
        return "mark";
      case MODEL:
        return "model";
      case CAR_BODY_TYPE:
        return "carBodyType";
      case COLOR:
        return "color";
      case ENGINE_TYPE:
        return "engineType";
      case GEARBOX:
        return "gearbox";
      default:
        return null;
    }
  }

  private ValidationCase getEnum(FieldTypesDto fieldTypesDto) {
    switch (fieldTypesDto.getField()) {
      case "MARK":
        return ValidationCase.MARK;
      case "MODEL":
        return ValidationCase.MODEL;
      case "CAR_BODY_TYPE":
        return ValidationCase.CAR_BODY_TYPE;
      case "COLOR":
        return ValidationCase.COLOR;
      case "ENGINE_TYPE":
        return ValidationCase.ENGINE_TYPE;
      case "GEARBOX":
        return ValidationCase.GEARBOX;
      default:
        return null;
    }
  }
}