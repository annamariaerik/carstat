package ee.datanor.carstat.service;

import com.google.common.collect.Lists;
import ee.datanor.carstat.dao.ICarDao;
import ee.datanor.carstat.domain.Car;
import ee.datanor.carstat.dto.SearchCarDto;
import ee.datanor.carstat.exception.Error;
import ee.datanor.carstat.exception.NotFoundException;
import ee.datanor.carstat.exception.ValidationException;
import ee.datanor.carstat.repository.CarRepository;
import ee.datanor.carstat.util.SearchCriteria;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Service
@Component
public class CarService {

    @Autowired
    CarRepository carRepository;

    @Autowired
    private ICarDao api;

    public List<Car> findByMarkIgnoreCaseAndModelIgnoreCaseAndIssueYear(String mark, String model, Integer issueYear) {
        return carRepository.findByMarkIgnoreCaseAndModelIgnoreCaseAndIssueYear(mark, model, issueYear);
    }

    public List<Car> findByColorAndMarkIgnoreCaseAndIssueYear(String color, String mark, Integer issueYear) {
        if (color != null && mark != null && issueYear != null && !mark.isEmpty() && !color.isEmpty()) {
            return carRepository.findByColorAndMarkIgnoreCaseAndIssueYear(color, mark, issueYear);
        }
        if (color != null && mark != null && !mark.isEmpty() && !color.isEmpty()) {
            return carRepository.findByColorAndMarkIgnoreCase(color, mark);
        }
        if (issueYear != null && mark != null && !mark.isEmpty()) {
            return carRepository.findByMarkIgnoreCaseAndIssueYear(mark, issueYear);
        }
        if (color != null && !color.isEmpty() && issueYear != null) {
            return carRepository.findByColorAndIssueYear(color, issueYear);
        }
        if (mark != null && !mark.isEmpty()) {
            return carRepository.findByMarkIgnoreCase(mark);
        }
        if (color != null && !color.isEmpty()) {
            return carRepository.findByColor(color);
        }
        if (issueYear != null) {
            return carRepository.findByIssueYear(issueYear);
        } else {
            return null;
        }
    }

    public SearchCarDto findAll(String search, int page, int limit) {

        int fullCarListSize = Lists.newArrayList(carRepository.findAll()).size();

        List<SearchCriteria> params = new ArrayList<SearchCriteria>();
        if (search != null) {
            Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)([a-zA-Z0-9_ äöõüÄÖÕÜ]+?),");
            Matcher matcher = pattern.matcher(search + ",");
            while (matcher.find()) {
                params.add(new SearchCriteria(matcher.group(1),
                        matcher.group(2), matcher.group(3)));
            }
        }

        if (api.searchCar(params, page, limit).getTotal() == fullCarListSize) {
            throw new ValidationException("Oops! Something went wrong, try to search something else!", Error.Code.NOT_FOUND);
        }

        SearchCarDto searchCarDto = new SearchCarDto();
        searchCarDto.setTotal(api.searchCar(params, page, limit).getTotal());
        searchCarDto.setResult(api.searchCar(params, page, limit).getResult());
        return searchCarDto;
    }
}