package ee.datanor.carstat.controller;

import ee.datanor.carstat.service.CarUpdateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/car")
@Slf4j
public class CarUpdateController {

    @Autowired
    CarUpdateService carUpdateService;

    @RequestMapping(value = "/{mark}", method = RequestMethod.PUT)
    public String getCaCarInfo(@PathVariable String mark,
                               @RequestParam(required = true, value = "model") String model,
                               @RequestParam(required = true, value = "year") String year) {
        carUpdateService.updateCarWeight(mark, model, year);
        return "Andmebaasis lisati kaal " + carUpdateService.updateCarWeight(mark, model, year) + " autole.";
    }
}