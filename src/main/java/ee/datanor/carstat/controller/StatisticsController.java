package ee.datanor.carstat.controller;

import ee.datanor.carstat.dto.SearchCarDto;
import ee.datanor.carstat.repository.CarStatisticsRepository;
import ee.datanor.carstat.service.CarService;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@CrossOrigin("*")
@RestController
@RequestMapping("/statistics")
public class StatisticsController {

    @Autowired
    CarStatisticsRepository carStatisticsRepository;

    @Autowired
    CarService carService;

    @RequestMapping(value = "/findCar", method = RequestMethod.GET)
    public SearchCarDto findCars(
            @RequestParam(value = "mark", required = false) String mark,
            @RequestParam(value = "model", required = false) String model,
            @RequestParam(value = "issueYear", required = false) Integer issueYear,
            @RequestParam(value = "color", required = false) String color,
            @RequestParam(value = "carBodyType", required = false) String carBodyType,
            @RequestParam(value = "engineType", required = false) String engineType,
            @RequestParam(value = "enginePower", required = false) Integer enginePower,
            @RequestParam(defaultValue = "1") int page,
            @RequestParam(defaultValue = "20") int limit) {
        log.debug("Su otsingule on " + carStatisticsRepository.findCars(mark, model, issueYear, color, carBodyType, engineType, enginePower, page, limit).getTotal() + " tulemust.");
        return carStatisticsRepository.findCars(mark, model, issueYear, color, carBodyType, engineType, enginePower, page, limit);
    }

    //    test http://localhost:8080/carstat-api/statistics/cars?search=mark:ASTON,issueYear>2010,color:hall
    @RequestMapping(method = RequestMethod.GET, value = "/cars")
    public SearchCarDto findAll(@ApiParam(value = "Otsi nt 'mark:AUDI,issueYear>2015'") @RequestParam(value = "search") String search,
                                @RequestParam(defaultValue = "1") int page,
                                @RequestParam(defaultValue = "20") int limit) {
        log.debug("Su otsingule on " + carService.findAll(search, page, limit).getTotal() + " tulemust.");
        return carService.findAll(search, page, limit);
    }
}