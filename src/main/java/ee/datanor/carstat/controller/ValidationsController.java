package ee.datanor.carstat.controller;

import ee.datanor.carstat.domain.FieldTypes;
import ee.datanor.carstat.dto.FieldTypesDto;
import ee.datanor.carstat.repository.FieldTypesRepository;
import ee.datanor.carstat.service.ValidateDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/validations")
public class ValidationsController {

    @Autowired
    ValidateDataService validateDataService;

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    public void addValidation(@RequestBody FieldTypesDto fieldTypesDto) {
        validateDataService.addValidation(fieldTypesDto);
    }
}