package ee.datanor.carstat.controller;

import ee.datanor.carstat.repository.CarRepository;
import ee.datanor.carstat.service.FileReaderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("/importcars")
public class ImportCarsController {
    @Autowired
    FileReaderService fileReaderService;

    @Autowired
    CarRepository carRepository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getCars() throws IOException {
        fileReaderService.importCar();
        return "Andmebaasis on " + carRepository.count() + " kirjet.";
    }
}