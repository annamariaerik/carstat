package ee.datanor.carstat.controller;

import ee.datanor.carstat.dto.Chart.CarTreeMapDto;
import ee.datanor.carstat.service.ChartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/statistics")
public class ChartController {
    @Autowired
    ChartService chartService;

    @RequestMapping(method = RequestMethod.GET, value = "/treemap")
    public CarTreeMapDto findAllMarkModelYear(@RequestParam(value = "map name", required = true) String mapName) {
        return chartService.findAllMarkModelYear(mapName);
    }
}