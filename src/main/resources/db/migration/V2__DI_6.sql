CREATE SEQUENCE field_type_seq start 1 increment 1;

CREATE TABLE IF NOT EXISTS field_types (
   id bigint PRIMARY KEY DEFAULT nextval('field_type_seq'),
   field character varying(50) NOT NULL,
   field_value character varying(50) NOT NULL,
   replace character varying(50) NOT NULL
);

INSERT INTO field_types (field, field_value, replace)
VALUES
('ENGINE_TYPE', 'PETROL', 'Bensiin'),
('ENGINE_TYPE', 'PETROL', 'Bensiin kat.');