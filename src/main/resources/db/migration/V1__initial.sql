CREATE SEQUENCE car_seq start 1 increment 1;

CREATE TABLE IF NOT EXISTS car (
    id bigint PRIMARY KEY DEFAULT nextval('car_seq'),
    mark character varying(50) NOT NULL,
    model character varying(50) NOT NULL,
    car_body_type character varying(50) NOT NULL,
    issue_year integer NOT NULL,
    color character varying(50) NOT NULL,
    engine_type character varying(50) NOT NULL,
    engine_volume integer NOT NULL,
    engine_power integer NOT NULL,
    gearbox character varying(50),
    quantity integer NOT NULL
);

