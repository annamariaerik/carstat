package ee.datanor.carstat.service;

import ee.datanor.carstat.dao.ICarDao;
import ee.datanor.carstat.domain.Car;
import ee.datanor.carstat.exception.NotFoundException;
import ee.datanor.carstat.exception.ValidationException;
import ee.datanor.carstat.repository.CarRepository;
import ee.datanor.carstat.util.SearchCriteria;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CarServiceTest {

    @Mock
    CarRepository carRepository;

    @Mock
    ICarDao api;

    @Spy
    @InjectMocks
    private CarService carService;

    private Car car1 = new Car();
    private List<Car> carList = new ArrayList<>();

    @Before
    public void init() {
        car1.setMark("SEAT");
        car1.setColor("must");
        car1.setIssueYear(2018);
        carList.add(car1);
    }

    @Test
    public void findByColorAndMarkIgnoreCaseAndIssueYearTest() {
        String mark = "SEAT";
        String color = "must";
        int issueYear = 2018;

        when(carRepository.findByColorAndMarkIgnoreCaseAndIssueYear(color, mark, issueYear)).thenReturn(carList);
        List<Car> cars = carService.findByColorAndMarkIgnoreCaseAndIssueYear(color, mark, issueYear);
        assertEquals(1, cars.size());
        assertEquals(mark, cars.get(0).getMark());
        assertEquals(color, cars.get(0).getColor());
        assertEquals(issueYear, cars.get(0).getIssueYear());
    }

    @Test
    public void findByMarkIgnoreCaseAndIssueYearTest() {
        String mark = "SEAT";
        String color = null;
        int issueYear = 2018;

        when(carRepository.findByMarkIgnoreCaseAndIssueYear(mark, issueYear)).thenReturn(carList);
        List<Car> cars = carService.findByColorAndMarkIgnoreCaseAndIssueYear(color, mark, issueYear);
        assertEquals(1, cars.size());
        assertEquals(mark, cars.get(0).getMark());
        assertEquals(issueYear, cars.get(0).getIssueYear());
    }

    @Test
    public void findByColor() {
        String mark = null;
        String color = "must";
        Integer issueYear = null;

        when(carRepository.findByColor(color)).thenReturn(carList);
        List<Car> cars = carService.findByColorAndMarkIgnoreCaseAndIssueYear(color, mark, issueYear);
        assertEquals(1, cars.size());
        assertEquals(color, cars.get(0).getColor());
    }

    @Test
    public void findNoneTest() {
        List<Car> carListEmpty = new ArrayList<>();
        String mark = "MAZDA";
        String color = "hall";
        int issueYear = 2018;
        when(carRepository.findByColorAndMarkIgnoreCaseAndIssueYear(color, mark, issueYear)).thenReturn(carListEmpty);
        List<Car> cars = carService.findByColorAndMarkIgnoreCaseAndIssueYear(color, mark, issueYear);
        assertEquals(0, cars.size());
    }

    @Test
    public void findAllTest() {
        String search = "color:must";
        SearchCriteria searchCriteria = new SearchCriteria("color", ":", "must");
        List<SearchCriteria> params = new ArrayList<>();
        params.add(searchCriteria);

        when(api.searchCar(params)).thenReturn(carList);

        List<Car> cars = carService.findAll(search);
        assertEquals(1, cars.size());
        assertEquals("must", cars.get(0).getColor());
    }

    @Test(expected = NotFoundException.class)
    public void findAllShouldThrowNotFoundExceptionTest() {
        List<Car> carListEmpty = new ArrayList<>();
        String search = "color:kollane";
        SearchCriteria searchCriteria = new SearchCriteria("color", ":", "kollane");
        List<SearchCriteria> params = new ArrayList<>();
        params.add(searchCriteria);

        when(api.searchCar(params)).thenReturn(carListEmpty);
        carService.findAll(search);
    }

    @Test(expected = ValidationException.class)
    public void findAllShouldThrowExceptionTest() {
        String search = "mark:ALFA ROMEO";

        Car car2 = new Car();
        car2.setMark("ALFA ROMEO");
        car2.setColor("hall");
        car2.setIssueYear(2016);
        carList.add(car2);

        when(carRepository.findAll()).thenReturn(carList);
        when(api.searchCar(anyList())).thenReturn(carList);

       carService.findAll(search);
    }
}