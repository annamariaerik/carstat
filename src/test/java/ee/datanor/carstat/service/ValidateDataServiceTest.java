package ee.datanor.carstat.service;

import ee.datanor.carstat.domain.Car;
import ee.datanor.carstat.domain.FieldTypes;
import ee.datanor.carstat.dto.FieldTypesDto;
import ee.datanor.carstat.exception.ValidationException;
import ee.datanor.carstat.repository.FieldTypesRepository;
import ee.datanor.carstat.util.ValidationCase;
import org.junit.runner.RunWith;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ValidateDataServiceTest {

    @Mock
    public FieldTypesRepository fieldTypesRepository;

    @Spy
    @InjectMocks
    private ValidateDataService validateDataService;

    private FieldTypesDto fieldTypesDtoDummy = new FieldTypesDto();
    private FieldTypesDto fieldTypesDtoDummy1 = new FieldTypesDto();
    private FieldTypesDto fieldTypesDtoDummy2 = new FieldTypesDto();
    private FieldTypes fieldTypesExpected = new FieldTypes();
    private FieldTypes fieldTypesDummy2 = new FieldTypes();
    private FieldTypes fieldTypesDummy3 = new FieldTypes();
    private List<FieldTypes> fieldTypesList = new ArrayList<>();
    private Car car = new Car();
    private Car carExpected = new Car();

    @Before
    public void init() {
        fieldTypesDtoDummy.setField("ENGINE_TYPE");
        fieldTypesDtoDummy.setFieldValue("PETROL");
        fieldTypesDtoDummy.setReplace("Diisel");

        fieldTypesDtoDummy2.setField("ENGINE_TYPE");
        fieldTypesDtoDummy2.setFieldValue("PETROL");
        fieldTypesDtoDummy2.setReplace("Bensiin");

        fieldTypesExpected.setField(ValidationCase.ENGINE_TYPE);
        fieldTypesExpected.setFieldValue("PETROL");
        fieldTypesExpected.setReplace("Diisel");

        fieldTypesDummy2.setField(ValidationCase.ENGINE_TYPE);
        fieldTypesDummy2.setFieldValue("PETROL");
        fieldTypesDummy2.setReplace("Bensiin");
        fieldTypesList.add(fieldTypesDummy2);

        fieldTypesDummy3.setField(ValidationCase.MARK);
        fieldTypesDummy3.setFieldValue("HONDA");
        fieldTypesDummy3.setReplace("ACURA");
        fieldTypesList.add(fieldTypesDummy3);

        car.setMark("ACURA");
        car.setEngineType("Bensiin");

        carExpected.setMark("HONDA");
        carExpected.setEngineType("PETROL");
    }

    @Test
    public void convertFieldTypesDtoToFieldTypesAndSaveTest() {
        when(fieldTypesRepository.save(fieldTypesExpected)).thenReturn(fieldTypesExpected);
        assertEquals(fieldTypesExpected, validateDataService.convertFieldTypesDtoToFieldTypesAndSave(fieldTypesDtoDummy));
    }

    @Test(expected = ValidationException.class)
    public void fieldTypesDtoGetFieldNotEnumTest() {
        fieldTypesDtoDummy1.setField("engineType");
        fieldTypesDtoDummy1.setFieldValue("PETROL");
        fieldTypesDtoDummy1.setReplace("Bensiin");

        validateDataService.convertFieldTypesDtoToFieldTypesAndSave(fieldTypesDtoDummy1);
    }

    @Test(expected = ValidationException.class)
    public void addDuplicateReplaceFieldTypesTest() {
        when(fieldTypesRepository.findAll()).thenReturn(fieldTypesList);
        validateDataService.convertFieldTypesDtoToFieldTypesAndSave(fieldTypesDtoDummy2);
    }

    @Test
    public void validateClassificatorsTest() {
        when(fieldTypesRepository.findAll()).thenReturn(fieldTypesList);
        assertEquals(carExpected, validateDataService.validateClassificators(car));
    }
}