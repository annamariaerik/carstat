import React from 'react';
import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom';
import { AppBar, Toolbar, Hidden, IconButton } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { makeStyles } from '@material-ui/styles';
import DriveEtaOutlinedIcon from '@material-ui/icons/DriveEtaOutlined';

const useStyles = makeStyles(theme => ({
  iconLink: {
    textDecoration: 'none'
  },
  icon: {
    marginLeft: theme.spacing(1)
  },
    iconLabel: {
    color: theme.palette.white,
    display: 'flex',
    fontSize: '1'
  },
   iconTitle: {
    margin: '0'
  },
  flexGrow: {
    flexGrow: 1
  }
}));


const Topbar = props => {
  const classes = useStyles();
  
  const {onSidebarOpen} = props;
  
  return (
    <AppBar>
      <Toolbar>

        <Hidden lgUp>
          <IconButton
            color="inherit"
            onClick={onSidebarOpen}
          >
            <MenuIcon />
          </IconButton>
        </Hidden>
                <div className={classes.flexGrow} />
        <div>
          <RouterLink to="/" className={classes.iconLink}>
            <IconButton className={classes.iconLabel}>
              <h5 className={classes.iconTitle}>CARSTAT</h5>
              <DriveEtaOutlinedIcon className={classes.icon} />
            </IconButton>
          </RouterLink>
        </div>
      </Toolbar>
    </AppBar>
  );
};

Topbar.propTypes = {
  onSidebarOpen: PropTypes.func
};

export default Topbar;
