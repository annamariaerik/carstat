import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Drawer } from '@material-ui/core';
import DashboardIcon from '@material-ui/icons/Dashboard';
import SearchIcon from '@material-ui/icons/Search';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { Hidden, IconButton } from '@material-ui/core';
import { SidebarNav } from './components';

const useStyles = makeStyles(theme => ({
  drawer: {
    width: 180,
    [theme.breakpoints.up('lg')]: {
      marginTop: 64,
      height: 'calc(100% - 64px)'
    }
  },
  root: {
    backgroundColor: theme.palette.white,
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    padding: theme.spacing(2)
  },
  nav: {
    marginBottom: theme.spacing(2)
  }
}));

const Sidebar = props => {
  const { open, variant, onClose, className, ...rest } = props;

  const classes = useStyles();
  
  const handleDrawerClose = () => {
    onClose(true);
  };

  const pages = [
    {
      title: 'Graafik',
      href: '/graafik',
      icon: <DashboardIcon />
    },
    {
      title: 'Otsing',
      href: '/otsing',
      icon: <SearchIcon />
    },
    {
      title: 'Otsing 2',
      href: '/otsing2',
      icon: <SearchIcon />
    }
  ];

  return (
    <Drawer
      anchor="left"
      classes={{ paper: classes.drawer }}
      onClose={onClose}
      open={open}
      variant={variant}
    >
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      <Hidden lgUp>
         <div className={classes.toolbarIcon}>
          <IconButton 
          onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
      </Hidden>
      <SidebarNav
        className={classes.nav}
        pages={pages}
      />
    </div>
    </Drawer>
  );
};

Sidebar.propTypes = {
  className: PropTypes.string,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
  variant: PropTypes.string.isRequired
};

export default Sidebar;
