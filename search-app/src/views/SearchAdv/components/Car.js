import React from 'react';
import {
  TableCell,
  TableRow
} from '@material-ui/core';

const Car = ({ mark, year, color, model, volume, gearbox, power, bodyType, type, quantity}) => {
  return (
      <TableRow>
        <TableCell>{mark}</TableCell>
        <TableCell>{model}</TableCell>
        <TableCell>{year}</TableCell>
        <TableCell>{bodyType}</TableCell>
        <TableCell>{type}</TableCell>
        <TableCell>{power}</TableCell>
        <TableCell>{volume}</TableCell>
        <TableCell>{color}</TableCell>
        <TableCell>{gearbox}</TableCell>
        <TableCell>{quantity}</TableCell>
      </TableRow>
  );
}
export default Car;