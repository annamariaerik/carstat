import React from 'react';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';

export default (props) => {
	const {
		      showPrevLink,
		      showNextLink,
		      handlePrevClick,
		      handleNextClick,
		      loading,
	      } = props;

	return (
		<div className="nav-link-container">
			<a
				href="#"
				className={`
				nav-link 
				${ showPrevLink ? 'show' : 'hide'}
				${ loading ? 'greyed-out' : '' }
				`}
				onClick={handlePrevClick}
				>
				<ChevronLeftIcon />
			</a>
			<a
				href="#"
				className={`
				nav-link
				${showNextLink ? 'show' : 'hide'}
				${ loading ? 'greyed-out' : '' }
				`}
				onClick={handleNextClick}
				>
				<ChevronRightIcon />
			</a>
		</div>
	);
};