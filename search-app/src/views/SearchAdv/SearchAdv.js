import React from 'react';
import CarList from './components/CarList';
import PageNavigation from './components/PageNavigation';
import axios from 'axios';
import Loader from './components/loader.gif';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Button,
  Grid,
  TextField,
  InputAdornment
} from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear';

function isInteger(text) {
    if (text == null) {
        return false;
    }
    let regex = /^\d+$/;
    return regex.test(text);
}

export class Search2 extends React.Component {
  constructor( props ) {
    super( props );

    this.state = {
      mark: '',
      model: '',
      issueYear1: '',
      issueYear2: '',
      enginePower1: '',
      enginePower2: '',
      engineVolume1: '',
      engineVolume2: '',
      gearbox: '',
      carBodyType: '',
      engineType: '',
      color: '',
      results: {},
      loading: false,
      message: '',
      totalResults: 0,
      totalPages: 0,
      currentPageNo: 0
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  clearSearch = (event) => {
    this.setState({
      mark: '',
      model: '',
      issueYear1: '',
      issueYear2: '',
      enginePower1: '',
      enginePower2: '',
      carBodyType: '', 
      engineVolume1: '',
      engineVolume2: '',
      gearbox: '',
      engineType: '',
      color: '',
      results: {},
      loading: false,
      message: '',
      totalResults: 0,
      totalPages: 0,
      currentPageNo: 0
    })
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value.toUpperCase();
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  fetchSearchResults = (updatedPageNo = '', query) => {
    const pageNumber = updatedPageNo ? `&page=${updatedPageNo}` : '';
    const searchUrl = `http://localhost:8080/carstat-api/statistics/cars?limit=20&${pageNumber}&search=${query}`;

    axios
      .get(searchUrl, {
      })
      .then((res) => {
        const total = res.data.total;
        const divisible = total % 20 === 0;
        const valueToBeAdded = divisible ? 0 : 1;
        const totalPagesCount = Math.floor(total / 20) + valueToBeAdded;
        const resultMsg = ! res.data.result.length ? "Tulemusi sellisele otsingule polnud! Proovi otsida midagi muud" : 
        (res.data.total > 1 ? 'Otsingule leiti ' + res.data.total + ' tulemust' : 'Otsingule leiti ' + res.data.total + ' tulemus')
        ;
        this.setState({
          results: res.data.result,
          message: resultMsg,
          toatalResults: total,
          totalPages: totalPagesCount,
          currentPageNo: updatedPageNo,
          loading: false
        })
      })
  }

  handleError = (event) => {
      let errorMsg = '';
      if ((this.state.issueYear1 !== '' && !isInteger(this.state.issueYear1))
        || (this.state.issueYear2 !== '' && !isInteger(this.state.issueYear2))) {
        errorMsg='Aasta peab olema täisarv';
      }
      if ((this.state.enginePower1 !== '' && !isInteger(this.state.enginePower1)) 
        || (this.state.enginePower2 !== '' && !isInteger(this.state.enginePower2))) {
        errorMsg='Võimsus peab olema täisarv';
      }
      if ((this.state.engineVolume1 !== '' && !isInteger(this.state.engineVolume1))
        || (this.state.engineVolume2 !== '' && !isInteger(this.state.engineVolume2))) {
        errorMsg='Maht peab olema täisarv';
      }
      this.setState({ loading: false, message: errorMsg })
  }

  handleSubmit = (event) => {
      event.preventDefault();
      if ((this.state.issueYear1 !== '' && !isInteger(this.state.issueYear1))
        || (this.state.issueYear2 !== '' && !isInteger(this.state.issueYear2)) 
        || (this.state.enginePower1 !== '' && !isInteger(this.state.enginePower1)) 
        || (this.state.enginePower2 !== '' && !isInteger(this.state.enginePower2))
        || (this.state.engineVolume1 !== '' && !isInteger(this.state.engineVolume1))
        || (this.state.engineVolume2 !== '' && !isInteger(this.state.engineVolume2))) {
        this.handleError();
      } else {
      const carMark =  this.state.mark === '' ? '' : `mark:${this.state.mark},`;
      const carModel =  this.state.model === '' ? '' : `model:${this.state.model},`;
      const carYear1 =  this.state.issueYear1 === '' ? '' : `issueYear>${this.state.issueYear1},`;
      const carYear2 =  this.state.issueYear2 === '' ? '' : `issueYear<${this.state.issueYear2},`;
      const carPower1 =  this.state.enginePower1 === '' ? '' : `enginePower>${this.state.enginePower1},`;
      const carPower2 =  this.state.enginePower2 === '' ? '' : `enginePower<${this.state.enginePower2},`;
      const carEngine =  this.state.engineType === '' ? '' : `engineType:${this.state.engineType},`;
      const carVolume1 =  this.state.engineVolume1 === '' ? '' : `engineVolume>${this.state.engineVolume1},`;
      const carVolume2 =  this.state.engineVolume2 === '' ? '' : `engineVolume<${this.state.engineVolume2},`;
      const carBody =  this.state.carBodyType === '' ? '' : `carBodyType:${this.state.carBodyType},`;
      const carGearbox =  this.state.gearbox === '' ? '' : `gearbox:${this.state.gearbox},`;
      const carColor =  this.state.color === '' ? '' : `color:${this.state.color}`;

      const query = carMark + carModel + carYear1 + carYear2 + carPower1 + carPower2 + carVolume1 + carVolume2 + carEngine + carGearbox + carBody + carColor;

      this.setState({ query, loading: true, message: '' }, () => {
        this.fetchSearchResults(1, query);
      });
    }
  }

  handlePageClick = (type) => {

    const updatedPageNo =
            'prev' === type
              ? this.state.currentPageNo - 1
              : this.state.currentPageNo + 1;

    if (!this.state.loading) {
      this.setState({ loading: true, message: '' }, () => {
        // Fetch previous 20 S
        this.fetchSearchResults(updatedPageNo, this.state.query);
      });
    }
  };

  renderSearchResults = () => {
    const {results} = this.state;

    if (Object.keys(results).length && results.length) {
      return (
        <div className="results-container">
          <CarList cars={results} />
        </div>
      );
    }
  };


  render() {
    const { loading, message, currentPageNo, totalPages } = this.state;
    const showPrevLink = 1 < currentPageNo;
    const showNextLink = totalPages > currentPageNo;
    
    return (
      <div >
        <Grid
          container
          justify="center"
          alignItems="center"
        > 
          <Grid
            item
            xs={12}
            md={10}
            style={{ marginTop: '3rem', marginBottom:'3rem' }}
          >
             <Card>
              <form onSubmit={this.handleSubmit}>
                <CardHeader
                  subheader="Täida vaid soovitud väljad"
                  title="Otsi Eesti liiklusregistris arvel olevaid sõiduautosid"
                />
                <Divider />
                <CardContent>
                  <Grid
                    container
                    spacing={3}
                    direction="row"
                    justify="flex-start"
                    alignItems="flex-start"
                  >
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={4}
                    >
                      <TextField
                        fullWidth
                        label="Mark"
                        name="mark"
                        type="text"
                        variant="outlined"
                        value = {this.state.mark}
                        onChange={this.handleChange}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={4}
                    >
                      <TextField
                        fullWidth
                        label="Mudel"
                        name="model"
                        type="text"
                        variant="outlined"
                        value = {this.state.model}
                        onChange={this.handleChange}
                      />  
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={4}
                    >
                      <TextField
                        fullWidth
                        label="Mootori tüüp"
                        name="engineType"
                        variant="outlined"
                        type="text"
                        value = {this.state.engineType}
                        onChange={this.handleChange}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={4}
                    >
                      <TextField
                        fullWidth
                        label="Keretüüp"
                        name="carBodyType"
                        variant="outlined"
                        type="text"
                        value = {this.state.carBodyType}
                        onChange={this.handleChange}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={4}
                    >
                      <TextField
                        fullWidth
                        label="Värvus"
                        name="color"
                        type="text"
                        variant="outlined"
                        value = {this.state.color}
                        onChange={this.handleChange}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={4}
                    >
                      <TextField
                        fullWidth
                        label="Käigukast"
                        name="gearbox"
                        type="text"
                        variant="outlined"
                        value = {this.state.gearbox}
                        onChange={this.handleChange}
                      />
                    </Grid>  
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={4}
                      container
                    >
                    <Grid
                      item
                      xs={12}
                      style={{paddingBottom: '1rem'}}
                    >
                    Registreerimisaasta
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      style={{paddingRight: '1rem'}}
                    >
                      <TextField
                        label="Alates"
                        fullWidth
                        name="issueYear1"
                        variant="outlined"
                        type="text"
                        onChange={this.handleChange}
                        value={this.state.issueYear1}
                      />
                      </Grid>
                      <Grid
                      item
                      xs={6}
                    >
                      <TextField
                        label="Kuni"
                        fullWidth
                        name="issueYear2"
                        variant="outlined"
                        type="text"
                        onChange={this.handleChange}
                        value={this.state.issueYear2}
                      />
                      </Grid>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={4}
                      container
                    >
                    <Grid
                      item
                      xs={12}
                      style={{paddingBottom: '1rem'}}
                   >
                    Mootori võimsus
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      style={{paddingRight: '1rem'}}
                    >
                      <TextField
                        label="Alates"
                        fullWidth
                        variant="outlined"
                        InputProps={{
                          endAdornment: <InputAdornment position="end">kW</InputAdornment>,
                        }}        
                        name="enginePower1"
                        type="text"
                        value = {this.state.enginePower1}
                        onChange={this.handleChange}
                      />
                       </Grid>
                      <Grid
                      item
                      xs={6}
                      >                     
                      <TextField
                        label="Kuni"
                        fullWidth
                        variant="outlined"
                        InputProps={{
                          endAdornment: <InputAdornment position="end">kW</InputAdornment>,
                        }} 
                        name="enginePower2"
                        type="text"
                        value = {this.state.enginePower2}
                        onChange={this.handleChange}
                      />
                    </Grid>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={4}
                      container
                    >
                    <Grid
                      item
                      xs={12}
                      style={{paddingBottom: '1rem'}}
                   >
                    Mootori maht
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      style={{paddingRight: '1rem'}}
                    >
                      <TextField
                        label="Alates"
                        fullWidth
                        variant="outlined"
                        InputProps={{
                          endAdornment: <InputAdornment position="end">cm³</InputAdornment>,
                        }} 
                        name="engineVolume1"
                        type="text"
                        value = {this.state.engineVolume1}
                        onChange={this.handleChange}
                      />
                       </Grid>
                      <Grid
                      item
                      xs={6}
                      >                     
                      <TextField
                        label="Kuni"
                        fullWidth
                        variant="outlined"
                        name="engineVolume2"
                        InputProps={{
                          endAdornment: <InputAdornment position="end">cm³</InputAdornment>,
                        }} 
                        type="text"
                        value = {this.state.engineVolume2}
                        onChange={this.handleChange}
                      />
                    </Grid>
                    </Grid>                                      
                  </Grid>
                </CardContent>
                <CardActions>
                  <Button
                    type="submit" 
                    color="primary"
                    variant="outlined"
                  >
                    Otsi
                  </Button>
                  <Button
                    onClick={this.clearSearch}
                  >
                    <ClearIcon /> Tühjenda otsing
                  </Button>
                </CardActions>
              </form>
                {/*Loader*/}
                <img  src={Loader} className={`search-loading ${loading ? 'show' : 'hide' }`}  alt="loader"/>
                {/*Message*/}
                { message && <h4 className="message">{message}</h4> }
                {/*Search Results*/}
                { this.renderSearchResults() }
                <PageNavigation
                  loading={loading}
                  showPrevLink={showPrevLink}
                  showNextLink={showNextLink}
                  handlePrevClick={() => this.handlePageClick('prev')}
                  handleNextClick={() => this.handlePageClick('next')}
                />
            </Card>
          </Grid>
        </Grid>
      </div>
      );
  }
}
