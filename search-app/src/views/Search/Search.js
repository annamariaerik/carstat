import React from 'react';
import CarList from './components/CarList';
import PageNavigation from './components/PageNavigation';
import axios from 'axios';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Button,
  Grid,
  TextField
} from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear';

export class Search extends React.Component {
  constructor( props ) {
    super( props );

    this.state = {
      mark: '',
      model: '',
      issueYear: '',
      enginePower: '',
      carBodyType: '',
      engineType: '',
      color: '',
      results: {},
      loading: false,
      message: '',
      totalResults: 0,
      totalPages: 0,
      currentPageNo: 0
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  clearSearch = (event) => {
    this.setState({
      mark: '',
      model: '',
      issueYear: '',
      enginePower: '',
      carBodyType: '',
      engineType: '',
      color: '',
      results: {},
      loading: false,
      message: '',
      totalResults: 0,
      totalPages: 0,
      currentPageNo: 0
    })
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value.toUpperCase();
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  fetchSearchResults = (updatedPageNo = '', query) => {
    const pageNumber = updatedPageNo ? `&page=${updatedPageNo}` : '';
    const searchUrl = `http://localhost:8080/carstat-api/statistics/findCar?limit=20${pageNumber}${query}`;

    axios
      .get(searchUrl, {
      })
      .then((res) => {

        const total = res.data.total;
        const divisible = total % 20 === 0;
        const valueToBeAdded = divisible ? 0 : 1;
        const totalPagesCount = Math.floor(total / 20) + valueToBeAdded;

        const resultMsg = ! res.data.result.length ? "Otsingutulemusi polnud! Proovi midagi muud" : 
        (res.data.total > 1 ? 'Otsingule leiti ' + res.data.total + ' tulemust' : 'Otsingule leiti ' + res.data.total + ' tulemus')
        ;
        this.setState({
          results: res.data.result,
          message: resultMsg,
          toatalResults: total,
          totalPages: totalPagesCount,
          currentPageNo: updatedPageNo,
          loading: false
        })
      })
  }

  handleSubmit = (event) => {
      event.preventDefault();
      const carMark =  this.state.mark === '' ? '' : `&mark=${this.state.mark}`;
      const carModel =  this.state.model === '' ? '' : `&model=${this.state.model}`;
      const carYear =  this.state.issueYear === '' ? '' : `&issueYear=${this.state.issueYear}`;
      const carPower =  this.state.enginePower === '' ? '' : `&enginePower=${this.state.enginePower}`;
      const carEngine =  this.state.engineType === '' ? '' : `&engineType=${this.state.engineType}`;
      const carBody =  this.state.carBodyType === '' ? '' : `&carBodyType=${this.state.carBodyType}`;
      const carColor =  this.state.color === '' ? '' : `&color=${this.state.color}`;

      const query = carMark + carModel + carYear + carPower + carEngine + carBody + carColor;

      this.setState({ query, loading: true, message: '' }, () => {
        this.fetchSearchResults(1, query);
      });
  }

  handlePageClick = (type) => {

    const updatedPageNo =
            'prev' === type
              ? this.state.currentPageNo - 1
              : this.state.currentPageNo + 1;

    if (!this.state.loading) {
      this.setState({ loading: true, message: '' }, () => {
        // Fetch previous 20 S
        this.fetchSearchResults(updatedPageNo, this.state.query);
      });
    }
  };

  renderSearchResults = () => {
    const {results} = this.state;

    if (Object.keys(results).length && results.length) {
      return (
        <div className="results-container">
          <CarList cars={results} />
        </div>
      );
    }
  };

  render() {
    const { loading, message, currentPageNo, totalPages } = this.state;
    const showPrevLink = 1 < currentPageNo;
    const showNextLink = totalPages > currentPageNo;
    
    return (
      <div >
        <Grid
          container
          justify="center"
          alignItems="center"
        > 
          <Grid
            item
            xs={12}
            sm={12}
            md={10}
            style={{ marginTop: '3rem', marginBottom:'3rem' }}
          >
             <Card>
              <form onSubmit={this.handleSubmit}>
                <CardHeader
                  subheader="Täida vaid soovitud väljad"
                  title="Otsi Eesti liiklusregistris arvel olevaid sõiduautosid"
                />
                <Divider />
                <CardContent>
                  <Grid
                    container
                    spacing={3}
                    direction="row"
                    justify="flex-start"
                    alignItems="flex-start"
                  >
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={3}
                      lg={3}
                    >
                      <TextField
                        fullWidth
                        label="Mark"
                        name="mark"
                        margin="dense"
                        type="text"
                        variant="outlined"
                        value = {this.state.mark}
                        onChange={this.handleChange}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={3}
                      lg={3}
                    >
                      <TextField
                        fullWidth
                        label="Mudel"
                        name="model"
                        type="text"
                        margin="dense"
                        variant="outlined"
                        value = {this.state.model}
                        onChange={this.handleChange}
                      />  
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={3}
                      lg={3}
                    >
                      <TextField
                        fullWidth
                        label="Aasta"
                        margin="dense"
                        name="issueYear"
                        type="text"
                        variant="outlined"
                        value = {this.state.issueYear}
                        onChange={this.handleChange}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={3}
                      lg={3}
                    >
                      <TextField
                        fullWidth
                        label="Mootori võimsus"
                        margin="dense"
                        name="enginePower"
                        type="text"
                        variant="outlined"
                        value = {this.state.enginePower}
                        onChange={this.handleChange}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={3}
                      lg={3}
                    >
                      <TextField
                        fullWidth
                        label="Mootori tüüp"
                        margin="dense"
                        name="engineType"
                        type="text"
                        variant="outlined"
                        value = {this.state.engineType}
                        onChange={this.handleChange}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={3}
                      lg={3}
                    >
                      <TextField
                        fullWidth
                        label="Keretüüp"
                        name="carBodyType"
                        margin="dense"
                        type="text"
                        variant="outlined"
                        value = {this.state.carBodyType}
                        onChange={this.handleChange}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={3}
                      lg={3}
                    >
                      <TextField
                        fullWidth
                        label="Värvus"
                        name="color"
                        type="text"
                        margin="dense"
                        variant="outlined"
                        value = {this.state.color}
                        onChange={this.handleChange}
                      />
                    </Grid>                                        
                  </Grid>
                </CardContent>
                <CardActions>
                  <Button
                    type="submit" 
                    color="primary"
                    variant="outlined"
                  >
                    Otsi
                  </Button>
                  <Button
                    onClick={this.clearSearch}
                  >
                    <ClearIcon /> Tühjenda otsing
                  </Button>
                </CardActions>
              </form>
                { message && <h4 className="message">{message}</h4> }
                { this.renderSearchResults() }
                <PageNavigation
                  loading={loading}
                  showPrevLink={showPrevLink}
                  showNextLink={showNextLink}
                  handlePrevClick={() => this.handlePageClick('prev')}
                  handleNextClick={() => this.handlePageClick('next')}
                />
            </Card>
          </Grid>
        </Grid>
      </div>
      );
  }
}
