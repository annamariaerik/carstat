import React from 'react';
import Car from './Car';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from '@material-ui/core';

const CarList = ({ cars }) => {
  return (
      <Table
        className='resultList'
        style={{minWidth: '650px' }}
      >
        <TableHead>
          <TableRow>
            <TableCell>Mark</TableCell>
            <TableCell>Model</TableCell>
            <TableCell>Keretüüp</TableCell>
            <TableCell>Mootor</TableCell>
            <TableCell>Võimsus</TableCell>
            <TableCell>Aasta</TableCell>
            <TableCell>Värvus</TableCell>
            <TableCell>Kogus</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {cars.map((carItem, i) => {
            return (
              <Car
                key={i}
                mark={cars[i].mark}
                model={cars[i].model}
                bodyType={cars[i].carBodyType}
                type={cars[i].engineType}
                power={cars[i].enginePower}
                year={cars[i].issueYear}
                color={cars[i].color}
                quantity={cars[i].quantity}
                />
              );
            })
          }
        </TableBody>
      </Table>
  );
}

export default CarList;