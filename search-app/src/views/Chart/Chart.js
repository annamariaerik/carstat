import React from 'react';
import TreeMap from 'react-d3-treemap';
import 'react-d3-treemap/dist/react.d3.treemap.css';
import { Grid, Card, CardHeader, CardContent, Divider } from '@material-ui/core';

export class Chart extends React.Component {

  state = {
    isLoading: true,
    treemap: {},
  };

  componentDidMount = (state, props) => {
    fetch('./data.json')
    .then(res => res.json())
    .then((data) => {
      this.setState({
        treemap: data,
        isLoading: false
      });
    })
  }

  render() {
    const { isLoading } = this.state;
    return (
      <div >
        <Grid
          container
          justify="center"
          alignItems="center"
        >
          <Grid
            item
            xs={12}
            md={10}
            style={{ marginTop: '3rem', marginBottom:'3rem' }}
          >
            {!isLoading ? 
              <div>
              <Card>
                <CardHeader
                  title="Eesti liiklusregistris arvel olevad sõiduautod seisuga 31.03.2019"
                />
                <Divider />
                 <CardContent>
                  <TreeMap
                    height={700}
                    width={850}
                    data={this.state.treemap}
                    valueUnit={"tk"}
                  />
                </CardContent>
                </Card>
              </div> : null
            }
          </Grid>
        </Grid>
      </div>
      );
  }
}