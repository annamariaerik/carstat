import React from 'react';
import { Switch, Redirect } from 'react-router-dom';
import { RouteWithLayout } from './components';
import { Main as MainLayout} from './layouts';
import {Chart} from './views/Chart/Chart';
import {Search} from './views/Search/Search';
import {Search2} from './views/SearchAdv/SearchAdv';

const Routes = () => {
  return (
    <Switch>
      <Redirect
        exact
        from="/"
        to="/graafik"
      />
      <RouteWithLayout
        component={Chart}
        exact
        layout={MainLayout}
        path="/graafik"
      />
      <RouteWithLayout
        component={Search}
        exact
        layout={MainLayout}
        path="/otsing"
      />
      <RouteWithLayout
        component={Search2}
        exact
        layout={MainLayout}
        path="/otsing2"
      />
    </Switch>
  );
};

export default Routes;
